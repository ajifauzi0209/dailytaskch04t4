package com.kazakimaru.dailytaskch04t4

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.kazakimaru.dailytaskch04t4.databinding.FragmentHomeBinding
import kotlin.system.exitProcess


class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPref: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPref = requireContext().getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)

        showDataSharedPref()
        doLogout()
    }

    private fun showDataSharedPref() {
        val username = sharedPref.getString("USERNAME", "Default Value")

        binding.txtNama.text = "Selamat datang $username"
    }

    private fun doLogout() {
        binding.btnLogout.setOnClickListener {
            val editor = sharedPref.edit()
            // Clear pada Shared Pref
            editor.clear()
            // Simpan Login State jadi FALSE
            editor.putBoolean("LOGIN_STATE", false)
            editor.apply()
            // Pindah ke Login Screen
            findNavController().navigate(R.id.action_homeFragment_to_loginFragment)
        }
    }
}