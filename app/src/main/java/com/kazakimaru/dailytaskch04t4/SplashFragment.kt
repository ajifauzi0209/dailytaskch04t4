package com.kazakimaru.dailytaskch04t4

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.kazakimaru.dailytaskch04t4.databinding.FragmentSplashBinding


class SplashFragment : Fragment() {

    private var _binding: FragmentSplashBinding? = null
    private val binding get() = _binding!!

    private lateinit var handler: Handler
    private lateinit var sharedPref: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSplashBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPref = requireContext().getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)

        checkSharedPref()
    }

    private fun checkSharedPref() {
        handler = Handler()
        handler.postDelayed({
            val stateLogin = sharedPref.getBoolean("LOGIN_STATE", false)

            if (!stateLogin) { // false = harus login
                findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
            } else { // true = langsung ke Home Screen
                findNavController().navigate(R.id.action_splashFragment_to_homeFragment)
            }
        }, 3000) // 3 detik
    }

}