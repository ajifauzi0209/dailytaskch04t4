package com.kazakimaru.dailytaskch04t4

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.setFragmentResult
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.kazakimaru.dailytaskch04t4.databinding.FragmentLoginBinding
import kotlin.system.exitProcess


class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPref: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPref = requireContext().getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)
//        checkSharedPref(view)
        doLogin()
    }

    // Jika pengecekkan shared pref di Login
//    private fun checkSharedPref(it: View) {
//        val stateLogin = sharedPref.getBoolean("LOGIN_STATE", false)
//        if (!stateLogin) { // false = harus login
//            Toast.makeText(requireContext(), "Login terlebih dahulu", Toast.LENGTH_SHORT).show()
//            doLogin()
//        } else { // true = langsung ke Home Screen
//            it.findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
//        }
//    }

    private fun doLogin() {
        binding.btnLogin.setOnClickListener {
            // Get value user & pass dari EditText
            val et_username = binding.editUsername.editText?.text.toString()
            val et_password = binding.editPassword.editText?.text.toString()

            // Cek inputan kosong
            if (loginValidation(et_username, et_password)) {
                // Editor SharedPref
                val editor = sharedPref.edit()

                // Save value EditText ke SharedPref
                editor.putString("USERNAME", et_username)
                editor.putString("PASS", et_password)
                // Simpan Login State jadi TRUE
                editor.putBoolean("LOGIN_STATE", true)

                editor.apply()

                // Pindah ke Home Screen
                findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
            }
        }
    }

    private fun loginValidation(username: String, password: String): Boolean {
        var result = true

        if (username.isEmpty()) {
            binding.editUsername.error = "Username tidak boleh kosong!"
            result = false
        } else {
            binding.editUsername.isErrorEnabled = false
        }

        if (password.isEmpty()) {
            binding.editPassword.error = "Password tidak boleh kosong!"
            result = false
        } else {
            binding.editPassword.isErrorEnabled = false
        }

        return result
    }

}